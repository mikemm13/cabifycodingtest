//
//  PriceRules.h
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, Discount){
    NoDiscount,
    Discount2x1,
    Discount3orMore
};

@interface PriceRules : NSObject

-(instancetype)initWithVoucherDiscount:(Discount)voucherDiscount tShirtDiscount:(Discount)tShirtDiscount mugDiscount:(Discount)mugDiscount;

@end
