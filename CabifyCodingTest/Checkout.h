//
//  Checkout.h
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 28/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PriceRules.h"
#import "Product.h"

@interface Checkout : NSObject

- (instancetype)initWithPriceRules:(PriceRules *)priceRules;
- (CGFloat)scan:(Product *)product;
- (void)resetValues;
@end
