//
//  ViewController.m
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import "ViewController.h"
#import "Checkout.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) Checkout *checkout;
@property (strong, nonatomic) PriceRules *priceRules;
@property (strong, nonatomic) Product *voucher;
@property (strong, nonatomic) Product *tShirt;
@property (strong, nonatomic) Product *mug;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (PriceRules *)priceRules {
    if (!_priceRules) {
        _priceRules = [[PriceRules alloc] initWithVoucherDiscount:Discount2x1 tShirtDiscount:Discount3orMore mugDiscount:NoDiscount];
    }
    return _priceRules;
}

- (Checkout *)checkout {
    if (!_checkout) {
        _checkout = [[Checkout alloc] initWithPriceRules:self.priceRules];
    }
    return _checkout;
}

- (Product *)voucher {
    if (!_voucher) {
        _voucher = [[Product alloc] initWithCode:@"VOUCHER" price:5.0];
    }
    return _voucher;
}

- (Product *)tShirt {
    if (!_tShirt) {
        _tShirt = [[Product alloc] initWithCode:@"TSHIRT" price:20.0];
    }
    return _tShirt;
}

- (Product *)mug {
    if (!_mug) {
        _mug = [[Product alloc] initWithCode:@"MUG" price:7.50];
    }
    return _mug;
}

- (IBAction)addVoucher:(id)sender {
    CGFloat total = [self.checkout scan:self.voucher];
    [self updatePriceLabelWithTotal:total];
}

- (IBAction)addTShirt:(id)sender {
    CGFloat total = [self.checkout scan:self.tShirt];
    [self updatePriceLabelWithTotal:total];
}

- (IBAction)addMug:(id)sender {
    CGFloat total = [self.checkout scan:self.mug];
    [self updatePriceLabelWithTotal:total];

}

- (IBAction)resetPressed:(id)sender {
    [self.checkout resetValues];
    [self updatePriceLabelWithTotal:0.0];
    
}

- (void) updatePriceLabelWithTotal:(CGFloat)total{
    self.priceLabel.text = [NSString stringWithFormat:@"%.02f €",total];
}


@end
