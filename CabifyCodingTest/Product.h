//
//  Product.h
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Product : NSObject

@property (strong, nonatomic) NSString *code;
@property (nonatomic) CGFloat price;

-(instancetype)initWithCode:(NSString *)code price:(CGFloat)price;

@end
