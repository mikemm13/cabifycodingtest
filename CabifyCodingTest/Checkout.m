//
//  Checkout.m
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 28/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import "Checkout.h"

@interface Checkout ()

@property (strong, nonatomic) PriceRules *priceRules;
@property (nonatomic) CGFloat totalPrice;
@property (nonatomic) NSInteger vouchersScanned;
@property (nonatomic) NSInteger tShirtsScanned;
@property (nonatomic) NSInteger mugsScanned;

@end

@implementation Checkout

- (instancetype)initWithPriceRules:(PriceRules *)priceRules {
    self = [super init];
    if (self) {
        _priceRules = priceRules;
    }
    return self;
}

- (CGFloat)scan:(Product *)product {
    self.totalPrice += product.price;
    if ([product.code isEqualToString:@"VOUCHER"]) {
        self.vouchersScanned += 1;
        if (self.vouchersScanned % 2 == 0) {
            self.totalPrice -= product.price;
        }
    } else if ([product.code isEqualToString:@"TSHIRT"]) {
        self.tShirtsScanned += 1;
        CGFloat tShirtsDiscount = 1.0;
        if (self.tShirtsScanned == 3) {
            self.totalPrice -= 3 * tShirtsDiscount;
        } else if (self.tShirtsScanned > 3) {
            self.totalPrice -= tShirtsDiscount;
        }
    }
    return self.totalPrice;
}

- (void)resetValues {
    self.totalPrice = 0.0;
    self.vouchersScanned = 0;
    self.tShirtsScanned = 0;

}

@end
