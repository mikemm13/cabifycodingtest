//
//  Product.m
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import "Product.h"

@implementation Product

- (instancetype)initWithCode:(NSString *)code price:(CGFloat)price {
    self = [super init];
    if (self) {
        _code = code;
        _price = price;
    }
    return self;
}

@end
