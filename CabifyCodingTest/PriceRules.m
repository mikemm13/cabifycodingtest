//
//  PriceRules.m
//  CabifyCodingTest
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import "PriceRules.h"

@interface PriceRules ()

@property (nonatomic) Discount voucherDiscount;
@property (nonatomic) Discount tShirtDiscount;
@property (nonatomic) Discount mugDiscount;


@end

@implementation PriceRules

- (instancetype)initWithVoucherDiscount:(Discount)voucherDiscount tShirtDiscount:(Discount)tShirtDiscount mugDiscount:(Discount)mugDiscount {
    self = [super init];
    if (self) {
        _voucherDiscount = voucherDiscount;
        _tShirtDiscount = tShirtDiscount;
        _mugDiscount = mugDiscount;
    }
    return self;
}

@end
