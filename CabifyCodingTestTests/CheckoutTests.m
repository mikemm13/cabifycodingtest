//
//  CabifyCodingTestTests.m
//  CabifyCodingTestTests
//
//  Created by Miguel Martin Nieto on 27/12/15.
//  Copyright © 2015 Miguel Martín-Nieto. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PriceRules.h"
#import "Checkout.h"
#import "Product.h"

@interface CheckoutTests : XCTestCase {

    Checkout *sut;
    Product *voucher;
    Product *tShirt;
    Product *mug;
}
@end

@implementation CheckoutTests

- (void)setUp {
    [super setUp];
    Discount voucherDiscount = Discount2x1;
    Discount tShirtDiscount = NoDiscount;
    Discount mugDiscount = Discount3orMore;

    PriceRules *priceRules = [[PriceRules alloc] initWithVoucherDiscount:voucherDiscount tShirtDiscount:tShirtDiscount mugDiscount:mugDiscount];
    sut = [[Checkout alloc] initWithPriceRules:priceRules];
    voucher = [[Product alloc] initWithCode:@"VOUCHER" price:5.0];
    tShirt = [[Product alloc] initWithCode:@"TSHIRT" price:20.0];
    mug = [[Product alloc] initWithCode:@"MUG" price:7.50];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    voucher = nil;
    tShirt = nil;
    mug = nil;
    sut = nil;
    [super tearDown];
}

- (void)testThatAddingOneVoucherReturnsVoucherUnitPrice {
    CGFloat total = [sut scan:voucher];
    XCTAssertEqual(total, voucher.price);
}

- (void)testThatAddingTwoVouchersReturnsVoucherUnitPrice {
    [sut scan:voucher];
    CGFloat total = [sut scan:voucher];
    XCTAssertEqual(total, voucher.price);
}

- (void)testThatAddingThreeVouchersReturnsPriceOfTwoVouchers {
    [sut scan:voucher];
    [sut scan:voucher];
    CGFloat total = [sut scan:voucher];
    XCTAssertEqual(total, 2 * voucher.price);
}

- (void)testThatAddingFourVouchersReturnsPriceOfTwoVouchers {
    [sut scan:voucher];
    [sut scan:voucher];
    [sut scan:voucher];
    CGFloat total = [sut scan:voucher];
    XCTAssertEqual(total, 2 * voucher.price);
}

- (void)testThatAddingOneTShirtReturnsTShirtUnitPrice {
    CGFloat total = [sut scan:tShirt];
    XCTAssertEqual(total, tShirt.price);
}

- (void)testThatAddingTwoTShirtsReturnsFullPriceOfTwoTShirts {
    [sut scan:tShirt];
    CGFloat total = [sut scan:tShirt];
    XCTAssertEqual(total, 2 * tShirt.price);
}

- (void)testThatAddingThreeTShirtsReturnsPriceWithOneEuroDiscountEach {
    [sut scan:tShirt];
    [sut scan:tShirt];
    CGFloat total = [sut scan:tShirt];
    CGFloat discount = 1.0;
    XCTAssertEqual(total, 3 * (tShirt.price - discount));
}

- (void)testThatAddingFourTShirtsReturnsPriceWithOneEuroDiscountEach {
    [sut scan:tShirt];
    [sut scan:tShirt];
    [sut scan:tShirt];
    CGFloat total = [sut scan:tShirt];
    CGFloat discount = 1.0;
    XCTAssertEqual(total, 4 * (tShirt.price - discount));
}

- (void)testThatAddingOneMugReturnsMugUnitPrice {
    CGFloat total = [sut scan:mug];
    XCTAssertEqual(total, mug.price);
}

- (void)testThatAddingTheThreeProductsReturnsSumOfPrices {
    [sut scan:voucher];
    [sut scan:tShirt];
    CGFloat total = [sut scan:mug];
    XCTAssertEqual(total, 32.50);
}

- (void)testThatAddingTwoVouchersAndOneTShirtReturns25 {
    [sut scan:voucher];
    [sut scan:tShirt];
    CGFloat total = [sut scan:voucher];
    XCTAssertEqual(total, 25.0);
}

- (void)testThatAddingFourTShirtsAndAVoucherReturns81 {
    [sut scan:tShirt];
    [sut scan:tShirt];
    [sut scan:tShirt];
    [sut scan:voucher];
    CGFloat total = [sut scan:tShirt];
    XCTAssertEqual(total, 81.0);
}

- (void)testThatAddingThreeVouchersThreeTShirtsAndAMugReturns74Point50{
    [sut scan:voucher];
    [sut scan:tShirt];
    [sut scan:voucher];
    [sut scan:voucher];
    [sut scan:mug];
    [sut scan:tShirt];
    CGFloat total = [sut scan:tShirt];
    XCTAssertEqual(total, 74.50);
}

- (void)testThatResetInitializeAllValues {
    [sut resetValues];
    XCTAssertEqual([[sut valueForKey:@"totalPrice"] floatValue] , 0.0);
    XCTAssertEqual([[sut valueForKey:@"vouchersScanned"] intValue] , 0);
    XCTAssertEqual([[sut valueForKey:@"tShirtsScanned"] intValue] , 0);
    
}

- (void)testThatResetInitializeAllValuesAfterAddingAProduct {
    [sut scan:voucher];
    [sut resetValues];
    XCTAssertEqual([[sut valueForKey:@"totalPrice"] floatValue] , 0.0);
    XCTAssertEqual([[sut valueForKey:@"vouchersScanned"] intValue] , 0);
    XCTAssertEqual([[sut valueForKey:@"tShirtsScanned"] intValue] , 0);

}

@end
